import os
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
	abort, render_template, flash
from contextlib import closing

app = Flask(__name__)

@app.route('/')
def start():
	return render_template('start.html')

@app.route('/insert', methods=['GET', 'POST'])
def insert():
	if request.method == 'POST':
		conn = sqlite3.connect('student.db')
		conn.execute('''insert into details values(?,?,?)''',
			(request.form['no'], request.form['name'], request.form['age']));
		conn.commit()
		conn.close()
		return redirect('/view')
	return render_template('insert.html')

@app.route('/view', methods=['GET', 'POST'])
def view():
	if request.method == 'GET':
		conn = sqlite3.connect('student.db')
		cursor = conn.execute("select * from details");
		rows = []
		for row in cursor:
			rows.append(row)
		conn.close()
		return render_template('view.html', rows=rows)

	else:
		return render_template('start.html')

@app.route('/search', methods=['GET', 'POST'])
def search():
	if request.method == 'POST':
		conn = sqlite3.connect('student.db')
		cursor = conn.execute("select * from details where name = (?)",(request.form['search'],))
		rows = []
		for row in cursor:
			rows.append(row)
		conn.close()
		return render_template('view.html', rows=rows)
	return render_template('search.html')
	
@app.route('/delete', methods = ['GET', 'POST'])
def delete():
	if request.method == 'POST':
		conn = sqlite3.connect('student.db')
		conn.execute("delete from details where name = (?)", (request.form['delete'],))
		conn.commit()
		conn.close()
		return redirect('/view')
	return render_template('delete.html')

app.run(debug=True)
